import { useContext, useState } from 'react';
import {Context} from '../store/RateContext';

const SearchBox = () =>{
    const {setQuery, setRate} = useContext(Context);
    const [search, setSearch] = useState("");

  const handleSubmit = (event) => {
    event.preventDefault();
    setRate(0)
    setQuery(search)
  }

  return (
    <form onSubmit={handleSubmit}>
      <label>Search for a movie:
        <input 
          type="text" 
          value={search}
          onChange={(e) => setSearch(e.target.value)}
        />
      </label>
      <input type="submit" value='search me' />
    </form>
  )
}

export default SearchBox;