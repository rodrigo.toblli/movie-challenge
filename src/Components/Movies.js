import React, {useContext, useEffect, useState} from 'react';
import noImage from './Assets/noImage.jpg';
import {Context} from '../store/RateContext';
import './Movies.css'

const Movies = ({ movieArray }) => {

    const {rate} = useContext(Context);
    const [movies, setMovies] = useState(movieArray);
    const [movieInfo, setmovieInfo] = useState(false)
    useEffect(()=>{
        if(rate !== 0){
            let filteredMovies=movieArray.filter(movie => movie.vote_average < rate*2 && movie.vote_average > rate*2-2);
            setMovies(filteredMovies)
        } else{
            setMovies(movieArray)
        }
    },[movieArray, rate])

    return (
        <div className='wrapper--movies'>
            {movies.length === 0 && <p className='no-movie'>No movies exist with this rating</p>}
            {movies.map(movie => {
                return (
                    <img key={movie.id}
                    className='img-movie' 
                    src={movie.poster_path ?
                        `https://image.tmdb.org/t/p/w200/${movie.poster_path}`:
                         noImage } 
                    alt={movie.original_title} onClick={()=>{setmovieInfo(movie)}}/>
                )
            })}
            <section className={movieInfo ? "selected-movie": "disable-movie"} onClick={()=>{setmovieInfo(false)}}>
                <p className="movie-info">{movieInfo.original_title}</p>
                <p className="movie-info">{movieInfo.overview}</p>
                <p className="movie-info">{movieInfo.release_date}</p>
            </section>
        </div>)
}

export default Movies;