import { useContext } from 'react';
import { Context } from '../store/RateContext';
import './Stars.css';

const Stars = () => {
    const { rate, setRate } = useContext(Context);
    return (
        <ul className='stars'>
            {Array.from({ length: 5 }, (_, i) =>
                <li
                    className={!(rate >= i + 1) ? 'disable' : undefined}
                    onClick={() => rate=== i + 1 ? setRate(0):setRate(i+1)}key={`star${i}`}>&#11088;
                </li>)}
        </ul>
    )
}

export default Stars;