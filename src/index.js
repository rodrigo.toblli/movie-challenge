import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import RateContext from './store/RateContext';

ReactDOM.render(
  <React.StrictMode>
    <RateContext> 
      <App />
    </RateContext>
  </React.StrictMode>,
  document.getElementById('root')
);
