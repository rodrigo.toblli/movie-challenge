import { createContext, useState } from "react";

export const Context = createContext();

const RateContext = ({children}) => {
    const [rate, setRate] = useState(0);
    const [query, setQuery] = useState('');
    return(
        <Context.Provider value={{rate, setRate, query, setQuery}} >
            {children}
        </Context.Provider>
    )
}

export default RateContext;