import './App.css';
import { useEffect, useState, useContext } from 'react';
import Movies from './Components/Movies';
import Stars from './Components/Stars';
import { Context } from './store/RateContext';
import SearchBox from './Components/SearchBox';

const App = () => {

  const { query } = useContext(Context);
  const BASE_URL = `https://api.themoviedb.org/3`;
  const API_KEY = "?api_key=abe27334798beb9b365976a5a1995773";
  const DISCOVER = `${BASE_URL}/discover/movie${API_KEY}&sort_by=popularity.desc`
  const SEARCH_QUERY = `${BASE_URL}/search/movie${API_KEY}&query=${query}`
  const [movies, setMovies] = useState(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    async function apiCall() {
      try {
        setLoading(true);
        let fetchType = query === '' ? DISCOVER : SEARCH_QUERY;
        let response = await fetch(fetchType)
        let responseJson = await response.json()
        setMovies(responseJson.results)
        setLoading(false)
      } catch (err) {
        alert(err)
      }
    }
    apiCall()
  }, [DISCOVER, SEARCH_QUERY, query]);

  return (
    <div className="App">
      <main className="App-header">
        <div className='stars-search'>
          <Stars />
          <SearchBox />
        </div>
        {loading ?
        <p className='wrapper--movies'>Loading...</p> :
        <Movies movieArray={movies} />}
      </main>
    </div>
  );
}

export default App;
